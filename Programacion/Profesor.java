import java.util.ArrayList;
import java.util.Scanner;

public class Profesor {
	private String nombre;
	private String Apellidos;
	private String Titulacion;
	private ArrayList<Asignatura> asignaturasProf= new ArrayList<Asignatura>();


	public Profesor(String nombre,String Apellidos, String Titulacion) {
		this.nombre= capit_nombre(nombre);
		this.Apellidos= capit_nombre(Apellidos);
		this.Titulacion=Titulacion.toUpperCase();
	}

	
	public String get_nombre() {
		
		return this.nombre;
		
	}
	
	public String get_Apellidos() {
		
		return this.Apellidos;
		
	}
	
	public ArrayList<Asignatura> get_asignaturas(){
		
		
		return this.asignaturasProf;
	}
	
	public String get_Titulacion() {
		
		return this.Titulacion;
		
	}
	
	public String capit_nombre(String nombre) {
		String[] cosas;
		String new_nombre="";
		cosas= nombre.split(" ");
		int num_cosas=0;
		if (cosas.length>0) {
			while (cosas.length > num_cosas){
				new_nombre=new_nombre+cosas[num_cosas].substring(0, 1).toUpperCase() + cosas[num_cosas].substring(1)+" ";
				num_cosas++;
			}
			return new_nombre;
		}
		else {
			
			return nombre;
		}
	}
	

	public void insert_asig_prof(ArrayList<Asignatura> asig) {
		Scanner sc =new Scanner(System.in);
		if (asig.size()>0) {
			System.out.println("A�adiendo asignaturas al profesor...");
			for(Asignatura asignaturas : asig) {
				System.out.println("Asignatura: "+asignaturas.get_name_asig());
				boolean asig_removed=false;
				for (Asignatura asigprof: asignaturasProf) {
					if (asigprof.get_name_asig().equals(asignaturas.get_name_asig())) {

						System.out.println("Desea eliminar esta asignatura en el profesor? si/no");
						String sino= sc.nextLine();
						if (sino.toLowerCase().equals("si")) {
							this.asignaturasProf.remove(asigprof);
							asig_removed=true;
							break;
						}
					}
				}
				if (asig_removed==false) {
					System.out.println("Desea insertar esta asignatura en el profesor? si/no");
					String sino= sc.nextLine();
					if (sino.toLowerCase().equals("si") ) {
						this.asignaturasProf.add(asignaturas);

					}
				}
			}
		}
		else {

			System.out.println("No existen asignaturas, debes crearlas antes!");

		}
	}


	public void edit_profesor(ArrayList<Asignatura> asig){

		Scanner sc =new Scanner(System.in);

		System.out.println("Quieres cambiar el nombre?:si/no");
		String sino=sc.nextLine();
		if (sino.toLowerCase().equals("si")){
			System.out.println("Introduce el nombre del profesor:");
			String nom_prof=sc.nextLine();
			this.nombre=nom_prof;
		}
		System.out.println("Quieres cambiar los apellidos?:si/no");
		sino=sc.nextLine();
		if (sino.toLowerCase().equals("si")){
			System.out.println("Introduce los apellidos del profesor:");
			String ape_prof=sc.nextLine();
			this.Apellidos=ape_prof;
		}
		System.out.println("Quieres cambiar la titulaci�n?:si/no");
		sino=sc.nextLine();
		if (sino.toLowerCase().equals("si")){
			System.out.println("Introduce la titulaci�n:");
			String titulo_prof=sc.nextLine();
			this.Titulacion=titulo_prof;
		}
		System.out.println("Quieres editar las asignaturas de este profesor?:si/no");
		sino=sc.nextLine();
		if (sino.toLowerCase().equals("si")){
			insert_asig_prof(asig);
		}

	}

	public String asig_prof(ArrayList<Asignatura> asig) {
		String asigna="";
		for(Asignatura asign : asig) {

			asigna=asigna+" " +asign.get_name_asig();

		}
		return asigna;



	}

	public void show_prof() {

		System.out.println("*********************");
		String asignaturas="";
		int num=0;
		for (Asignatura asigna: get_asignaturas()) {
			asignaturas = asignaturas +" / "+ asigna.get_name_asig();

		}
		asignaturas=asignaturas+" /";
	
		
		
		System.out.println("Profesor-  Nombre: "+this.nombre+ "  || Apellidos: "+ this.Apellidos+ " || Asignaturas que imparte: "+ asignaturas);
		System.out.println("*********************");

	}




}
