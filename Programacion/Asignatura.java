
public class Asignatura {

	private String nombre;
	Temario temario;
	private double precioAsignatura;

	public Asignatura(String nombre) {
		// TODO Auto-generated constructor stub
		this.nombre = capit_nombre(nombre);
		this.temario= new Temario(nombre);
		precio_Asignatura();
	}

	public String capit_nombre(String nombre) {
		String[] cosas;
		String new_nombre="";
		cosas= nombre.split(" ");
		int num_cosas=0;
		if (cosas.length>0) {
			while (cosas.length > num_cosas){
				new_nombre=new_nombre+cosas[num_cosas].substring(0, 1).toUpperCase() + cosas[num_cosas].substring(1)+" ";
				num_cosas++;
			}
			return new_nombre;
		}
		else {
			
			return nombre;
		}
	}

	public Asignatura() {
		this.nombre = "";
	}


	public void edit_Asig() {
		temario.edit_tema();
	}

	public void precio_Asignatura() {
		double precio = 3.755;
		int horasTotal;
		horasTotal = temario.horas_temario();
		double pricetotal= horasTotal*precio;
		this.precioAsignatura= pricetotal;
	}

	public String asig_price_eur() {
		int horasTotal;
		horasTotal = temario.horas_temario();
		double precio = 3.755;
		double pricetotal= horasTotal*precio;
		String str = String.format("%1.2f", pricetotal);
		return str;
	}

	public String horasTotal() {
		int horasTotal;
		horasTotal = temario.horas_temario();
		String horas= String.valueOf(horasTotal);
		return horas;
	}

	public void show_asig() {
		System.out.println("*********************");
		System.out.println("Asignatura: "+nombre+ "  || Precio: "+ asig_price_eur() );
		System.out.println("Total de horas: "+ horasTotal());
		temario.show_temario();
		System.out.println("*********************");
	}	

	public String get_name_asig() {
		return this.nombre;		
	}

	public void set_name_asig(String nombre) {
		this.nombre= nombre;
		temario.set_temario_name(nombre);
	}
}
