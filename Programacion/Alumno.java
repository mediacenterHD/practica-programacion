import java.util.ArrayList;
import java.util.Scanner;

public class Alumno {
	private String nombre;
	private String Apellidos;
	private String DNI;
	String FechaNacimiento;
	ArrayList<Matricula> matriculas= new ArrayList<Matricula>();

	public Alumno(String dni) {
		set_DNI(dni);
		set_nombre();
		set_apellidos();
		set_fech_nac();
		
		
	}

	public Matricula search_matricula(ArrayList<Matricula> matricu,String dni) {

		for (Matricula matri: matricu) {
			if(matri.get_DNI().toLowerCase().equals(dni)) {

				return matri;

			}

		}
		return null;

	}

	public void insert_matri_alumn(ArrayList<Matricula> matricu) {
		Scanner sc =new Scanner(System.in);
		if (matricu.size()>0) {
			System.out.println("A�adiendo matriculas al alumno con DNI: "+ this.DNI);
			Matricula matric_inser=search_matricula(matricu,this.DNI);
			if (matric_inser !=null) {
				System.out.println("A�adiendo matriculas al alumno con DNI: "+ this.DNI);

			}
		}
		else {

			System.out.println("No existen asignaturas, debes crearlas antes!");

		}
	}

	
	
	public Matricula alum_with_matri(ArrayList<Matricula> matriculas,String dni) {
		if (matriculas.size()>0) {
			for(Matricula matricula : matriculas) {
				if(matricula.get_DNI_sin().equals(dni)) {
					return matricula;
					
				}
				
			}
			return null;
		}
		else
			return null;
		
	}
	
	public void edit_alumno(ArrayList<Matricula> matriculas,String dni,ArrayList<Asignatura> asignaturas) {
		String sino;
		Scanner sc =new Scanner(System.in);
		System.out.println("Desea editar el DNI?si/no");
		sino= sc.nextLine();
		if (sino.toLowerCase().equals("si")) {
			
			set_DNI();
			
		}
		System.out.println("Desea editar el nombre?si/no");
		sino= sc.nextLine();
		if (sino.toLowerCase().equals("si")) {
			
			set_nombre();
			
		}
		System.out.println("Desea editar los apellidos?si/no");
		sino= sc.nextLine();
		if (sino.toLowerCase().equals("si")) {
			
			set_apellidos();
			
		}
		System.out.println("Desea editar la fecha de nacimiento?si/no");
		sino= sc.nextLine();
		if (sino.toLowerCase().equals("si")) {
			
			set_fech_nac();
			
		}
		
		Matricula nueva = alum_with_matri(matriculas,dni);
		if (nueva != null) {
			
			int pos_matr=matriculas.indexOf(nueva);
			matriculas.get(pos_matr).edit_matricula(asignaturas, dni);
			
		}
		
	}
	
	public void set_fech_nac() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce la fecha de nacimiento");
		String fecha= sc.nextLine();
		this.FechaNacimiento=fecha;
	}

	public void set_nombre() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce el nombre");
		String nombre= sc.nextLine();
		
		this.nombre=capit_nombre(nombre);
	}

	public String capit_nombre(String nombre) {
		String[] cosas;
		String new_nombre="";
		cosas= nombre.split(" ");
		int num_cosas=0;
		if (cosas.length>0) {
			while (cosas.length > num_cosas){
				new_nombre=new_nombre+cosas[num_cosas].substring(0, 1).toUpperCase() + cosas[num_cosas].substring(1)+" ";
				num_cosas++;
			}
			return new_nombre;
		}
		else {
			
			return nombre;
		}
	}
	
	public void set_apellidos() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce los Apellidos");
		String Apellidos= sc.nextLine();
		this.Apellidos=capit_nombre(Apellidos);
	}

	
	public void set_DNI(String dni) {
		boolean dni_puesto=false;
		Scanner sc =new Scanner(System.in);
		while (dni_puesto == false) {
			if (dni.length()>8) {
				System.out.println("El DNI introducido debe ser un n�mero entero positivo con 8 cifras como m�ximo");
				System.out.println("Introduce el dni de nuevo:");
				dni=sc.nextLine();
				
			}
			else if (esEnteroPositivo(dni)==false) {
	
				System.out.println("El DNI introducido debe ser un n�mero entero positivo.");
				System.out.println("Introduce el dni de nuevo:");
				dni=sc.nextLine();
			}else{
				
				System.out.println("NIF: " + dni+calculaLetra(Integer.parseInt(dni)));
				this.DNI=dni;
				dni_puesto=true;
			}
		}

	}
	
	public void set_DNI() {
		boolean dni_puesto=false;
		
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce el dni de nuevo:");
		String dni=sc.nextLine();
		while (dni_puesto == false) {
			if (dni.length()>8) {
				System.out.println("El DNI introducido debe ser un n�mero entero positivo con 8 cifras como m�ximo");
				System.out.println("Introduce el dni de nuevo:");
				dni=sc.nextLine();
				
			}
			else if (esEnteroPositivo(dni)==false) {
	
				System.out.println("El DNI introducido debe ser un n�mero entero positivo.");
				System.out.println("Introduce el dni de nuevo:");
				dni=sc.nextLine();
			}else{
				
				System.out.println("NIF: " + dni+calculaLetra(Integer.parseInt(dni)));
				this.DNI=dni;
				dni_puesto=true;
			}
		}

	}

	public String get_DNI_letra() {
		if (esEnteroPositivo(this.DNI)==false) {
			return "";
		}else{
			return this.DNI+calculaLetra(Integer.parseInt(this.DNI));
		}


	}
	public String get_DNI() {
		if (esEnteroPositivo(this.DNI)==false) {
			return "";
		}else{
			return this.DNI;
		}
	}

	public void show_alumno() {
		System.out.println("NIF: " + this.DNI+calculaLetra(Integer.parseInt(this.DNI))+" || Nombre: "+this.nombre+" || Apellidos: "+this.Apellidos+" || Fech Naci: "+this.FechaNacimiento);
		for (Matricula matri : matriculas) {
			if(this.DNI.equals(matri.get_DNI_sin())) {
				matri.show_matricula();
				break;
			}
		
		}
		
	}
	public char calculaLetra(int dni){
		char letras[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
		int resto = dni%23;
		return letras[resto];
	}

	public boolean esEnteroPositivo(String cadena){
		try{
			if(Integer.parseInt(cadena)>0) {
				return true;
			}else {
				return false;
			}
		}catch (Exception e) {
			return false;
		}
	}
}
