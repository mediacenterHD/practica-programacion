
public class Tema {


	private String nombre;
	private int horas;

	public Tema(String nombre, int horas) {
		// TODO Auto-generated constructor stub
		this.nombre= nombre;
		this.horas= horas;

	}

	public String nombre_tema() {

		return this.nombre;

	}
	public void set_nombre(String nombre) {

		this.nombre = nombre.toLowerCase();

	}

	public void set_hora(int hora) {

		this.horas= hora;

	}
	public int horas_tema() {

		return this.horas;

	}

}
