import java.util.Scanner;

public class Fecha {

	private int dia;
	private int mes;
	private int anyo;
	boolean bisiesto;
	
	public Fecha() {
		
		alta_fecha();
		
	}

	public void set_anyo() {
		int anyo=0;
		Scanner sc =new Scanner(System.in);
		while(anyo <1900 || anyo > 9999) {
			
			System.out.println("Introduce el a�o");
			 anyo= Integer.parseInt(sc.nextLine());
			if (anyo <1900 || anyo > 9999)
				System.out.println("Error!!(A�os aceptados 1900-9999)");
		}
		this.anyo= anyo;
		
	}
	
	public void alta_fecha() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Desea introducir la fecha a mano? (formato DD/MM/AAAA) si/no:");
		String sino= sc.nextLine();
		if (sino.toLowerCase().equals("si")) {
			System.out.println("Introduce la fecha (formato DD/MM/AAAA)");
			String fecha=sc.nextLine();
			String[] dia_mes_anyo;
			boolean fecha_conseguida=false;
			int dia=0 ;
			int mes=0 ;
			int anyo=0 ;
			boolean dia_f = false,mes_f=false,anyo_f=false;
			while (fecha_conseguida ==false) {
				try {
				dia_mes_anyo= fecha.split("/");
				dia = Integer.parseInt(dia_mes_anyo[0]);
				dia_f=true;
				mes = Integer.parseInt(dia_mes_anyo[1]);
				mes_f=true;
				String anyo_s=dia_mes_anyo[2];
				anyo = Integer.parseInt(anyo_s);
				anyo_f=true;
				fecha_conseguida=true;
				}catch(Exception e) {
					if (dia_f ==false) {
						set_dia();
					}
					else if (mes_f == false) {
						
						set_mes();
					}
					else if (anyo_f == false) {
						
						set_anyo();
					}
					fecha_conseguida=true;
				}
			}
			
			check_anyo(anyo);
			check_mes( mes);
			check_dia( dia);
		}
		else {
			set_anyo();
			set_mes();
			set_dia();
			
		}
	}
	
	public void check_dia(int dia){
		
		int mes_actual=get_mes();
		int anyo_actual=get_anyo();
		
		Scanner sc =new Scanner(System.in);
		if (mes_actual==2) {
			int dias_posibles= febrero(anyo_actual);
			
			while(dia <1 || dia > dias_posibles) {
				System.out.println("Error!! Introduce un dia entre 1 y"+dias_posibles);
				
				dia= Integer.parseInt(sc.nextLine());
				
					
			}
		}
		else if((mes_actual % 2)==1){
			while(dia <1 || dia >31) {
				System.out.println("Error!! Introduce un dia entre 1 y 31 ");
				dia= Integer.parseInt(sc.nextLine());
			}
		}
		else {
			
			while(dia <1 || dia >30) {
				System.out.println("Error!! Introduce un dia entre 1 y 30 ");
				dia= Integer.parseInt(sc.nextLine());
			}
			
		}
		this.dia=dia;
		System.out.println("Dia insertado correctamente");
		
	}
	
	public void check_mes(int mes){
		
		while(mes <1 || mes > 12) {
			System.out.println("Error!! ");
			System.out.println("Introduce un mes valido ( 1 a 12):");
			Scanner sc =new Scanner(System.in);
			mes= Integer.parseInt(sc.nextLine());
		}
		this.mes=mes;
		System.out.println("Mes insertado correctamente");
	}
	
	public void check_anyo(int anyo){
		
		while(anyo <1900 || anyo > 9999) {
			System.out.println("Error!!(A�os aceptados 1900-9999)");
			
			System.out.println("Introduce un a�o valido");
			Scanner sc =new Scanner(System.in);
			anyo= Integer.parseInt(sc.nextLine());
				
		}
		this.anyo= anyo;
		System.out.println("A�o correctamente insertado");
		
	}
	
	public void set_mes() {
		Scanner sc =new Scanner(System.in);
		int mes= 0;
		while(mes <1 || mes > 12) {
			
			System.out.println("Introduce un mes (valor del 1 a 12):");
			mes= Integer.parseInt(sc.nextLine());
			if (mes <1 || mes > 12)
				System.out.println("Error!!");
		}
		this.mes= mes;

	}
	
	public int get_mes() {
		
		return this.mes;
		
	}
	
	public int get_anyo() {
		
		return this.anyo;
		
	}
	
	public int get_dia() {
		
		return this.dia;
	}
	
	public void set_dia() {
		int mes_actual=get_mes();
		int anyo_actual=get_anyo();
		
		Scanner sc =new Scanner(System.in);
		if (mes_actual==2) {
			int dias_posibles= febrero(anyo_actual);
			int dia=0;
			while(dia <1 || dia > dias_posibles) {
				System.out.println("Introduce un dia");
				dia= Integer.parseInt(sc.nextLine());
				if (dia !=1 || dia != dias_posibles)
					System.out.println("Error!! Introduce un dia entre 1 y"+dias_posibles);
			}
		}
		else if((mes_actual % 2)==1){
			while(dia <1 || dia >31) {
				System.out.println("Introduce un dia");
				dia= Integer.parseInt(sc.nextLine());
				if (dia <1 || dia >31)
					System.out.println("Error!! Introduce un dia entre 1 y 31 ");
			}
		}
		else {
			
			while(dia <1 || dia >30) {
				System.out.println("Introduce un dia");
				dia= Integer.parseInt(sc.nextLine());
				if (dia <1 || dia >30)
					System.out.println("Error!! Introduce un dia entre 1 y 30 ");
			}
			
		}
		this.dia=dia;
	}
		
	public int febrero(int a�o) {
		int numDias;
		if ( ((a�o % 4 == 0) && !(a�o % 100 == 0)) || (a�o % 400 == 0) )
			numDias = 29;
		else
			numDias = 28;

		return numDias;


	}




}
