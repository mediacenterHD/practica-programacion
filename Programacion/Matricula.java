import java.util.ArrayList;
import java.util.Scanner;

public class Matricula {
	private String DNI;
	private String Nombre;
	ArrayList<Asignatura> asig_Matricula= new ArrayList<Asignatura>();
	ArrayList<String> notas=new ArrayList<String>();
	private String fecha;
	double precio_matricula;
	private Fecha fecha_alta;
	private String pagada="No pagada";

	public Matricula(ArrayList<Asignatura> asig,String dni) {
		set_DNI(dni);
		set_centro();
		set_Asignaturas_matricula(asig);
		fecha_alta= new Fecha();
	}

	public void set_centro() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce el centro educativo donde se ha realizado la matricula: ");
		String centro= sc.next();
		this.Nombre=centro;
	}


	public String get_DNI() {
		if (esEnteroPositivo(this.DNI)==false) {
			return "";
		}else{
			return this.DNI+calculaLetra(Integer.parseInt(this.DNI));
		}


	}

	public String get_DNI_sin() {
		if (esEnteroPositivo(this.DNI)==false) {
			return "";
		}else{
			return this.DNI;
		}


	}
	

	
	public void edit_matricula(ArrayList<Asignatura> asig,String dni) {
		
		if(get_matricula() == false) {
		
			Scanner sc =new Scanner(System.in);
			System.out.println("Deseas editar el DNI? si/no");
			String sino= sc.next();
			if(sino.toLowerCase().equals("si")) {
				set_DNI(dni);
	
			}
			System.out.println("Deseas editar el Centro educativo? si/no");
			sino= sc.next();
			if(sino.toLowerCase().equals("si")) {
				set_centro();
	
			}
			System.out.println("Deseas editar las asignaturas de esta matricula? si/no");
			sino= sc.next();
			if(sino.toLowerCase().equals("si")) {
				set_Asignaturas_matricula(asig);
			}
			System.out.println("Deseas editar el pago de la matricula? si/no");
			sino= sc.next();
			if(sino.toLowerCase().equals("si")) {
				if(get_matricula()==false) {
					matricula_pagada();
				}
			}
		}
		else {
			
			System.out.println("Matricula pagada, no se puede editar");
			
		}
	}

	public void set_Asignaturas_matricula(ArrayList<Asignatura> asig) {
		Scanner sc =new Scanner(System.in);
		if (asig.size()>0) {
			if (get_matricula()==false) {
				System.out.println("A�adiendo asignaturas a la matricula...");
				for(Asignatura asignaturas : asig) {
					System.out.println("Asignatura: "+asignaturas.get_name_asig());
					boolean asig_removed=false;
					for (Asignatura asigprof: asig_Matricula) {
						if (asigprof.get_name_asig().equals(asignaturas.get_name_asig())) {
							System.out.println("Ya esta a�adida esta asignatura,�Desea eliminar esta asignatura de la matricula? si/no");
							String sino= sc.next();
							if (sino.toLowerCase().equals("si")) {
								int pos_as=this.asig_Matricula.indexOf(asigprof);
								this.asig_Matricula.remove(asigprof);
								this.notas.remove(notas.get(pos_as));
								asig_removed=true;
								break;
							}
						}
					}
					if (asig_removed==false) {
						System.out.println("Desea insertar esta asignatura a la matricula? si/no");
						String sino= sc.next();
						if (sino.toLowerCase().equals("si") ) {
							this.asig_Matricula.add(asignaturas);
							int pos_as=this.asig_Matricula.indexOf(asignaturas);
							this.notas.add(set_nota());
						}
					}
				}
			}
			else {

				System.out.println("Esta matricula est� pagada,no se puede editar");
			}
		}
		else {

			System.out.println("No existen asignaturas, debes crearlas antes!");

		}


	}

	public void matricula_pagada() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Deseas asignar la matricula como pagada?si/no:");
		String sino= sc.next();
		if(sino.toLowerCase().equals("si")) {
			this.pagada="Pagada";
		}
		else {
			this.pagada="No pagada";

		}

	}

	public boolean get_matricula() {

		if(this.pagada.equals("Pagada"))
			return true;
		else
			return false;

	}


	public void calcular_precio_matricula(ArrayList<Asignatura> asig_Matricula) {
		double price_matricula=0;
		for (Asignatura asign : asig_Matricula) {
			price_matricula=price_matricula+Double.parseDouble(asign.asig_price_eur());
		}
		this.precio_matricula=price_matricula;

	}

	public void show_matricula() {
		String asig="";
		String nota;
		int pasada= 0;
		for (Asignatura asign :asig_Matricula) {
			nota =notas.get(pasada);
			asig=asig+ " || "+asign.get_name_asig() +" - nota: "+nota+" ";
			pasada=pasada+1;
		}
		System.out.println("Matricula para dni: " +get_DNI()+" || Centro: "+this.Nombre+" Asignaturas: "+asig);
		
	}
	
	public String set_nota() {

		Scanner sc =new Scanner(System.in);
		System.out.println("Desea asignar la nota para esta asignatura?si/no:");
		String sino=sc.next();
		if (sino.toLowerCase().equals("si")) {
			System.out.println("Introduce la nota para la asignatura");
			String nota= sc.next();
			return nota;
		}
		else
			return "";


	}



	public void set_DNI(String dni) {

		this.DNI=dni;

	}

	public char calculaLetra(int dni){
		char letras[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
		int resto = dni%23;
		return letras[resto];
	}

	public boolean esEnteroPositivo(String cadena){
		try{
			if(Integer.parseInt(cadena)>0) {
				return true;
			}else {
				return false;
			}
		}catch (Exception e) {
			return false;
		}
	}
}
