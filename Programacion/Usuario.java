import java.util.Scanner;

public class Usuario {
	private String email;
	private String Password;

	public Usuario() {
		// TODO Auto-generated constructor stub
		set_email();
		set_password();
		System.out.println("Usuario creado correctamente");
	}

	public void set_email() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Introduce el email");
		String email= sc.nextLine();
		this.email=email;


	}

	public String get_password() {

		return this.Password;
	}

	public String get_email() {
		return this.email;


	}

	public void set_password() {
		Scanner sc =new Scanner(System.in);
		String contrase�a="";
		int num_contr=contrase�a.length();
		while(num_contr<6) {
			System.out.println("Introduce una contrase�a (Minimo 6 caracteres entre los cuales debe haber una may�scula, una min�scula, y alg�n n�mero)");
			contrase�a= sc.nextLine();
			if (contrase�a.length()>6) {
				
				boolean mayus=false,minus=false,num=false;
				char letra;
				for (int i=0;i<contrase�a.length();i++) {
					letra = contrase�a.charAt(i);
					String encontrado = String.valueOf(letra);
					if (encontrado.matches("[A-Z]")) {
						mayus=true;
					} else if (encontrado.matches("[a-z]")) {
					   minus=true;
					} else if (encontrado.matches("[0-9]")) {
					    num=true;
					}
					
				}
		
				if(num==false || minus==false || mayus == false) {

					System.out.println("La contrase�a no cumple con los requisitos...");
					if (num == false) 
						System.out.println("Debe a�adir minimo un numero");
					if (minus == false)
						System.out.println("Debe a�adir minimo una letra en minuscula");
					if (mayus == false)
						System.out.println("Debe a�adir minimo una letra en mayuscula");
				}
				else {

					this.Password = contrase�a;
					num_contr=contrase�a.length();
				}
			}

			else {
				System.out.println("Error, contrase�a demasiado corta..");

			}
		}

	}


}
