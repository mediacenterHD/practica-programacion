
import java.util.ArrayList;
import java.util.Scanner;

import java.lang.reflect.Field;

public class CentroEducativo {

	private static Object Matricula;
	//static Clear clear= new Clear();
	public static void crearAsig(ArrayList<Asignatura> asig,String nom_Asig) {
		int numAsig=asig.size();
		if (numAsig != 0) {
			boolean same_name= false;
			for (Asignatura nombreAsig : asig ) {
				if (nombreAsig.get_name_asig().equals(nom_Asig)) {
					same_name= true;
					break;
				}
			}
			if (same_name==false) {

				Asignatura newAsig= new Asignatura(nom_Asig);
				asig.add(newAsig);
			}
			else {
				System.out.println("Asignatura existente, cree una nueva o editela si lo desea");
				same_name=false;
			}
		}
		else
		{
			Asignatura newAsig= new Asignatura(nom_Asig);
			asig.add(newAsig);
		}

	}

	public static Matricula buscar_matricula(ArrayList<Matricula> matriculas,String dni) {

		int numAsig=matriculas.size();
		if (numAsig != 0) {

			for (Matricula matricu : matriculas ) {
				if (matricu.get_DNI_sin().equals(dni)) {
					return matricu;
				}
			}
		}
		else
		{
			System.out.println("No existe matriculaci�n");
			return null;
		}

		return null;
	}

	public static Asignatura buscarAsig(ArrayList<Asignatura> asig,String nom_Asig) {
		int numAsig=asig.size();
		if (numAsig > 0) {
		
			for (Asignatura nombreAsig : asig ) {
				String nombre_asig=nombreAsig.get_name_asig().toLowerCase();
				if(nombre_asig.substring(nombre_asig.length()-1).equals(" ")) {
					nombre_asig=nombre_asig.substring(0, nombre_asig.length()-1);
				}
					
				if (nombre_asig.equals(nom_Asig.toLowerCase())) {
					return nombreAsig;
				}
			}
			System.out.println("No se ha encontrado ninguna asignatura con ese nombre, buscando coincidencias..");
			for (Asignatura nombreAsig : asig ) {
				
				if (nombreAsig.get_name_asig().toLowerCase().matches("(.*)"+nom_Asig+"(.*)")) {
					
					nombreAsig.show_asig();
					
				}
			}
			return null;
		}
		else
		{
			System.out.println("No existe asignaturas dadas de altas");
			return null;
		}


	}

	public static void edit_Asignatura(ArrayList<Asignatura> asig,String nom_Asig) {

		Asignatura Asigencontrada=buscarAsig( asig,nom_Asig);
		if (Asigencontrada != null) {
			Scanner sc =new Scanner(System.in);
			System.out.println("Desea cambiar el nombre de la Asignatura? si/no");
			String sino= sc.nextLine();

			if (sino.equals("si")) {
				System.out.println("Introduce el nuevo nombre");
				String new_name= sc.nextLine();
				Asigencontrada.set_name_asig(new_name);
			}
			System.out.println("Desea editar algun tema?si/no");
			sino= sc.nextLine();
			if (sino.equals("si")) {

				Asigencontrada.edit_Asig();
			}

		}

	}


	public static void show_user(ArrayList<Usuario> usuarios,String email) {

		Usuario user_encontrado=search_user(usuarios,email);
		int num= 1;
		if (user_encontrado !=null) {
			System.out.println(" Usuario "+num+" || Email: "+user_encontrado.get_email()+ " || Password: "+user_encontrado.get_password());
			num=num+1;
		}
		else {
			System.out.println("Usuario no encontrado ");
		}
	}


	
	public static void show_user_coincidencia(ArrayList<Usuario> usuarios, String email) {
		
		
		int numAsig=usuarios.size();
		int num= 0;
		if (numAsig != 0) {

			for (Usuario email_user : usuarios ) {
					
				if (email_user.get_email().matches("(.*)"+email+"(.*)")) {
					
					System.out.println(" Usuario "+num+" || Email: "+email_user.get_email()+ " || Password: "+email_user.get_password());
					num++;
					
				}
			}
			if (num ==0)
					System.out.println("Ningun usuario contiene parte del email introducido");
		
		}
		else
		{
			System.out.println("No existen usuarios creados");
		}



	}

	public static void edit_user(ArrayList<Usuario> usuarios,String email) {

		Usuario user_encontrado=search_user(usuarios,email);
		if (user_encontrado !=null) {

			Scanner sc =new Scanner(System.in);
			System.out.println("Desea cambiar email? si/no");
			String sino= sc.nextLine();

			if (sino.equals("si")) {
				user_encontrado.set_email();

			}

			System.out.println("Desea cambiar el password? si/no");
			sino= sc.nextLine();

			if (sino.toLowerCase().equals("si")) {
				user_encontrado.set_password();

			}
		}
		else
		{
			System.out.println("No se ha encontrado el usuario");

		}


	}

	public static void show_Asig(ArrayList<Asignatura> asig) {
		for (Asignatura select_asig : asig ) {
			select_asig.show_asig();

		}
	}

	public static void delete_asig(ArrayList<Asignatura> asig,String nom_Asig) {
		Asignatura Asig_encontrada= buscarAsig( asig,nom_Asig);
		if (Asig_encontrada != null) {

			asig.remove(Asig_encontrada);
			System.out.println("Asignatura eliminada correctamente");

		}

	}

	public static void new_user(ArrayList<Usuario> usuarios) {


		Usuario new_user= new Usuario();
		usuarios.add(new_user);
		System.out.println("Usuario a�adido");
	}
	
	public static Usuario search_user(ArrayList<Usuario> usuarios, String email) {

		int numAsig=usuarios.size();
		if (numAsig != 0) {

			for (Usuario email_user : usuarios ) {
				if (email_user.get_email().toLowerCase().equals(email.toLowerCase())) {
					return email_user;
				}
			}
			return null;
		}
		else
		{
			return null;
		}



	}

	public static void delete_user(ArrayList<Usuario> usuarios,String email) {
		Usuario user_encontrado=search_user(usuarios,email);
		if (user_encontrado !=null) {

			usuarios.remove(user_encontrado);
			System.out.println("Usuario eliminado correctamente");
		}
		else
		{
			System.out.println("No se ha encontrado el usuario");

		}

	}

	public static void show_Prof(ArrayList<Asignatura> asig, ArrayList<Profesor> profesores) {
		int num=0;
		if(profesores.size()>0) {
			for (Profesor prof : profesores) {
				String asignaturas="";

				for (Asignatura asigna: prof.get_asignaturas()) {
					asignaturas = asignaturas +" / "+ asigna.get_name_asig();

				}
				asignaturas=asignaturas+" /";
				num=num+1;
				System.out.println("Profesor "+String.valueOf(num)+" ||Nombre: "+prof.get_nombre()+" || Apellidos: "+prof.get_Apellidos()+" "+" || Asignaturas: "+asignaturas);

			}
			System.out.println("");
		}
		else {

			System.out.println("No existen profesores");
		}
	}

	public static void show_coinc_prof(ArrayList<Profesor> profesores,ArrayList<Asignatura> asig, String nombre) {
		
			int num=0;
			if(profesores.size()>0) {
				for (Profesor prof : profesores) {
					String asignaturas="";
					if (prof.get_nombre().toLowerCase().matches("(.*)"+nombre.toLowerCase()+"(.*)")) {
						for (Asignatura asigna: prof.get_asignaturas()) {
							asignaturas = asignaturas +" / "+ asigna.get_name_asig();
	
						}
						asignaturas=asignaturas+" /";
						num=num+1;
						System.out.println("Profesor "+String.valueOf(num)+" ||Nombre: "+prof.get_nombre()+" || Apellidos: "+prof.get_Apellidos()+" "+" || Asignaturas: "+asignaturas);
					}
				}

			}
		
	}
	
	
	public static void edit_asig_prof(ArrayList<Profesor> profesores,ArrayList<Asignatura> asig, String nombre) {
		boolean prof_encontrado= false;
		if( profesores.size() >0){
			
			for(Profesor prof : profesores) {
				String nombr_prof= prof.get_nombre().toLowerCase();
				if(nombr_prof.substring(nombr_prof.length()-1).equals(" ")) {
					nombr_prof=nombr_prof.substring(0, nombr_prof.length()-1);
				}
				if(nombr_prof.equals(nombre.toLowerCase())) {
					prof.show_prof();
					prof.edit_profesor(asig);
					prof_encontrado= true;
					break;
					}
				}
			
			
			if (prof_encontrado==false) {
				System.out.println("No se ha encontrado ningun profesor con ese nombre, buscando coincidencias..");
				show_coinc_prof(profesores,asig,nombre);
			}
		}
		else {
			System.out.println("No existen profesores dados de alta");
		}
		
	}
	
	public static void insert_asig_prof(ArrayList<Profesor> profesores,ArrayList<Asignatura> asig, String nombre) {
		boolean prof_encontrado= false;
		if( profesores.size() >0){
			
			for(Profesor prof : profesores) {
				String nombr_prof= prof.get_nombre().toLowerCase();
				if(nombr_prof.substring(nombr_prof.length()-1).equals(" ")) {
					nombr_prof=nombr_prof.substring(0, nombr_prof.length()-1);
				}
				if(nombr_prof.equals(nombre.toLowerCase())) {
					prof.show_prof();
					prof.insert_asig_prof(asig);
					prof_encontrado= true;
					break;
					}
				}
			
			
			if (prof_encontrado==false) {
				System.out.println("No se ha encontrado ningun profesor con ese nombre, buscando coincidencias..");
				show_coinc_prof(profesores,asig,nombre);
			}
		}
		else {
			System.out.println("No existen profesores dados de alta");
		}
		
	}
	
	public static void search_prof(ArrayList<Profesor> profesores,ArrayList<Asignatura> asig, String nombre) {
		boolean prof_encontrado= false;
		if( profesores.size() >0){

			for(Profesor prof : profesores) {
				if(prof.get_nombre().toLowerCase().equals(nombre.toLowerCase())) {
					prof.show_prof();
					prof_encontrado=true;
				}

			}
			if (prof_encontrado==false) {
				System.out.println("No se ha encontrado ningun profesor con ese nombre, buscando coincidencias..");
				show_coinc_prof(profesores,asig,nombre);
			}
		}
		else {
			System.out.println("No existen profesores dados de alta");
		}
	}

	public static void new_alumno(ArrayList<Alumno> alumnos_tot,String dni) {
		Alumno new_alumno= new Alumno(dni);
		alumnos_tot.add(new_alumno);
		System.out.println("Alumno creado");
	}

	public static Alumno buscar_alumno(ArrayList<Alumno> alumnos,String dni) {
		if (alumnos.size()>0){
			for(Alumno alumno : alumnos) {
				if(dni.equals(alumno.get_DNI())==true) {

					return alumno;
				}
			}
			return null;
		}
		else {
			return null;
		}

	}

	public static boolean exist_elemento(int num,ArrayList<Asignatura> asig,ArrayList<Profesor> profesores,ArrayList<Usuario> usuarios,ArrayList<Alumno> alumnos) {

		String tipo="";
		if (num ==1) {
			tipo="Usuarios";
			if(usuarios.size()>0)
				return true;

		}
		if (num ==2) {
			tipo="Alumnos";
			if(alumnos.size()>0)
				return true;
		}
		if (num ==3) {
			tipo="Profesores";
			if(profesores.size()>0)
				return true;
		}
		if (num ==4) {
			tipo="Asignaturas";
			if(asig.size()>0)
				return true;
		}
		System.out.println("No existe "+tipo+" dado de alta");
		return false;
	}

	public static void exist_alumno(ArrayList<Alumno> alumnos,String dni) {
		if (alumnos.size()>0){
			for(Alumno alumno : alumnos) {
				if(dni.equals(alumno.get_DNI())==true) {
					System.out.println("Ya existe un alumno con ese DNI");
					break;
				}
			}
			new_alumno(alumnos,dni);
		}
		else {
			new_alumno(alumnos,dni);
		}
	}

	public static void main(String[] args) {
		ArrayList<Asignatura> asig=new ArrayList<Asignatura>();
		ArrayList<Profesor> profesores=new ArrayList<Profesor>();
		ArrayList<Usuario> usuarios=new ArrayList<Usuario>();
		ArrayList<Alumno> alumnos=new ArrayList<Alumno>();
		ArrayList<Matricula> matriculas=new ArrayList<Matricula>();
		Scanner sc =new Scanner(System.in);
		String nom_Asig;
		int menu= -1,usu_menu=0,alu_menu=0,pro_menu=0,asig_menu=0;
		boolean mene=false,usumenu=false,alumen=false,promenu=false,asigmenu=false;
		while (mene == false ) {
			System.out.println("");
			System.out.println("************************");
			System.out.println("GESTI�N DE CENTRO EDUCATIVO");
			System.out.println("***********************************");
			System.out.println("1. Usuarios");
			System.out.println("2. Alumnos");
			System.out.println("3. Profesores");
			System.out.println("4. Asignaturas");
			System.out.println("0. Salir");
			menu= Integer.parseInt(sc.nextLine());
			switch(menu) {
			case 1:
				usumenu = false;
				while (usumenu ==false) {
					//clear.clear();
					System.out.println("");
					System.out.println("************************");
					System.out.println(" GESTI�N DE USUARIOS");
					System.out.println("************************");
					System.out.println("1. Crear");
					System.out.println("2. Editar");
					System.out.println("3. Eliminar");
					System.out.println("4. Buscar");
					System.out.println("5. Buscar coincidencia");
					System.out.println("0. Volver al menu anterior");
					usu_menu= Integer.parseInt(sc.nextLine());
					switch(usu_menu) {
					case 1 :
						new_user(usuarios);
						break;
					case 2 :
						if (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduzca el email del usuario que desea editar: ");
							String email_user=sc.nextLine();
							edit_user(usuarios,email_user);
						}
						break;
					case 3 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduzca el email del usuario que desea eliminar ");
							String email_user_delete=sc.nextLine();
							delete_user(usuarios,email_user_delete);
						}
						break;
					case 4 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduzca el email del usuario a buscar: ");
							String search_user=sc.nextLine();
							show_user(usuarios,search_user);
						}
						break;
					case 5 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduzca parte del email del usuario a buscar: ");
							String search_user=sc.nextLine();
							show_user_coincidencia(usuarios,search_user);
						}
						break;
					case 0 :System.out.println("Volviendo a menu anterior");
					usumenu= true;
					break;
					}
				}
				break;
			case 2:
				alumen = false;
				while ( alumen == false) {
					System.out.println("");
					System.out.println("************************");
					System.out.println(" GESTI�N DE ALUMNOS");
					System.out.println("************************");
					System.out.println("1. Crear");
					System.out.println("2. Editar");
					System.out.println("3. Eliminar");
					System.out.println("4. Buscar");
					System.out.println("5. Matricular");
					System.out.println("0. Volver al menu anterior");
					alu_menu= Integer.parseInt(sc.nextLine());
					switch(alu_menu) {
					case 1 :
						System.out.println("Introduce el DNI del alumno a crear(sin letra):");
						String dni= sc.nextLine();
						exist_alumno(alumnos, dni);
						break;
					case 2  :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el DNI del alumno a editar:");
							String dni_bus= sc.nextLine();
							Alumno alumn =buscar_alumno(alumnos, dni_bus);
							if( alumn!=null) {
								int pos_alu= alumnos.indexOf(alumn);
								alumnos.get(pos_alu).edit_alumno(matriculas, dni_bus,asig);
							}
						}
						break;
					case 3 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el DNI del alumno a eliminar:");
							String dni_elim= sc.nextLine();
							Alumno delte_elumn =buscar_alumno(alumnos, dni_elim);
							if( delte_elumn!=null) {
								Matricula matr=buscar_matricula(matriculas,dni_elim);
								if (matr!=null) {
									int pos_matr=matriculas.indexOf(matr);
									matriculas.get(pos_matr).notas.remove(pos_matr); //eliminamos la nota de la matricula
									matriculas.remove(matr);  //eliminamos la matricula
								}
								alumnos.remove(delte_elumn); //finalmente eliminamos el alumno
							}
							else
								System.out.println("Alumno no encontrado");
						}
						break;
					case 4 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el DNI del alumno a buscar:");
							String dni1= sc.nextLine();
							Alumno new_alum=buscar_alumno(alumnos, dni1);
							if (new_alum!=null)
								new_alum.show_alumno();
							else
								System.out.println("Alumno no encontrado");
						}
						break;
					case 5 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							if (asig.size()>0) {
							System.out.println("Introduce el DNI del alumno a Matricular:");
							String dni1= sc.nextLine();
							Alumno new_alum=buscar_alumno(alumnos, dni1);
							if (new_alum!=null) {
								if (matriculas.size()==0) {
									Matricula matriculita= new Matricula(asig,dni1);
									matriculas.add(matriculita);
								}
								else {
									Matricula matr=buscar_matricula(matriculas,dni1);
									if (matr!=null) {
										System.out.println("Alumno con matricula ya creada,desea editar la matricula?si/no:");
										String sino = sc.nextLine();
										if (sino.toLowerCase().equals("si")) {
											int pos_matr=matriculas.indexOf(matr);
											matriculas.get(pos_matr).edit_matricula(asig, dni1);
										}
									}
									else {

										Matricula matriculita= new Matricula(asig,dni1);
										matriculas.add(matriculita);
									}
								}
							}
							else
								System.out.println("Alumno no encontrado");
							}
							else
								System.out.println("No existen asignaturas dadas de alta");
							
						}
						break;
					case 0 :System.out.println("Volviendo a menu anterior");
					alumen = true;
					break;

					}
				}
				break;
			case 3:
				promenu=false;
				while (promenu == false) {
					System.out.println("");
					System.out.println("************************");
					System.out.println(" GESTI�N DE PROFESORES");
					System.out.println("************************");
					System.out.println("1. Crear");
					System.out.println("2. Editar");
					System.out.println("3. Eliminar");
					System.out.println("4. Buscar");
					System.out.println("5. Agregar Asignaturas");
					System.out.println("6. Mostrar Todos");
					System.out.println("0. Volver al menu anterior");
					pro_menu= Integer.parseInt(sc.nextLine());
					int pos_prof;
					switch(pro_menu) {
					case 1 :

						System.out.println("Creando profesor..");
						System.out.println("Introduce el nombre del profesor:");
						String nom_prof=sc.nextLine();
						System.out.println("Introduce los apellidos del profesor:");
						String ape_prof=sc.nextLine();
						System.out.println("Introduce la titulaci�n del profesor:");
						String tit_prof=sc.nextLine();
						Profesor profesor = new Profesor(nom_prof,ape_prof,tit_prof);
						profesores.add(profesor);
						break;

					case 2 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							
							System.out.println("Introducir nombre(n) o mostrar lista(l)? (n/l)");
							String n_l = sc.nextLine();
							if(n_l.toLowerCase().equals("l")) {
								
								show_Prof(asig,profesores);
								System.out.println("Introduce el numero del profesor que quieres editar");
								pos_prof=Integer.parseInt(sc.nextLine());
								if(profesores.size()>pos_prof-1 && pos_prof >0){
									profesores.get(pos_prof-1).edit_profesor(asig);
									}
								else {
									System.out.println("Posicion erronea!!");
								}
									
								}
							else if (n_l.toLowerCase().equals("n")) {
								System.out.println("Introduce el nombre del profesor a editar: ");
								String nom_prof1=sc.nextLine();
								edit_asig_prof(profesores,asig, nom_prof1);
								
							}
							else {
								
								System.out.println("Introduce un valor valido!!");
							}
						}
						break;
					case 3 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							show_Prof(asig,profesores);
							System.out.println("Introduce el numero del profesor que quieres eliminar");
							pos_prof=Integer.parseInt(sc.nextLine());
							profesores.remove(profesores.get(pos_prof-1));
						}
						break;
					case 4 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el nombre del profesor a buscar: ");
							String nom_prof1=sc.nextLine();
							search_prof(profesores,asig, nom_prof1);
						}
						break;
					case 5 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introducir nombre(n) o mostrar lista(l)? (n/l)");
							String n_l = sc.nextLine();
							if(n_l.toLowerCase().equals("l")) {
								
								show_Prof(asig,profesores);
								System.out.println("Introduce el numero del profesor al que quieras a�adir asignaturas");
								pos_prof=Integer.parseInt(sc.nextLine());
								if(profesores.size()>pos_prof-1 && pos_prof >1){
									profesores.get(pos_prof-1).insert_asig_prof(asig);}
								else {
									System.out.println("Posicion erronea!!");
								}
									
								}
							else if (n_l.toLowerCase().equals("n")) {
								System.out.println("Introduce el nombre del profesor a a�adir asignaturas");
								String nom_prof1=sc.nextLine();
								insert_asig_prof(profesores,asig, nom_prof1);
								
							}
							else {
								
								System.out.println("Introduce un valor valido!!");
							}
							
							
						}
						break;
					case 6 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							show_Prof(asig,profesores);
						}
						break;
					case 0 :System.out.println("Volviendo a menu anterior");
					promenu=true;
					break;

					}
				}
				break;
			case 4:
				asigmenu=false;
				while(asigmenu == false) {
					System.out.println("");
					System.out.println("************************");
					System.out.println(" GESTI�N DE ASIGNATURAS");
					System.out.println("************************");
					System.out.println("1. Crear");
					System.out.println("2. Editar");
					System.out.println("3. Eliminar");
					System.out.println("4. Buscar");
					System.out.println("5. Mostrar Todo");
					System.out.println("0. Volver al menu anterior");
					asig_menu= Integer.parseInt(sc.nextLine());
					switch(asig_menu) {
					case 1 :
						
						System.out.println("Creando asignatura..");
						System.out.println("Introduce el nombre de la asignatura");
						nom_Asig=sc.nextLine();
						crearAsig(asig, nom_Asig);
						break;

					case 2 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el nombre de la asignatura a editar");
							nom_Asig=sc.nextLine();
							edit_Asignatura(asig,nom_Asig);
						}
						break;

					case 3 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el nombre de la asignatura a eliminar");
							nom_Asig=sc.nextLine();
							delete_asig(asig,nom_Asig);
						}
						break;
					case 4 :
						if  (exist_elemento(menu,asig,profesores,usuarios,alumnos)){
							System.out.println("Introduce el nombre de la asignatura a buscar");
							nom_Asig=sc.nextLine();
							Asignatura asignatura=buscarAsig(asig,nom_Asig);
							if (asignatura != null)
							{

								System.out.println("Asignatura encontrada, mostrando sus datos");
								asignatura.show_asig();
							}
							else {

								System.out.println("No se ha encontrado la asignatura");
							}
						}
						break;
					case 5 :
						show_Asig(asig);
						break;
					case 0 :System.out.println("Volviendo a menu anterior");
					asigmenu=true;
					break;

					}
				}
				break;
			case 0: System.out.println("Saliendo...");
			mene=false;
			break;
			default: System.out.println("Opcion no valida");
			break;

			}
		}
		System.out.println("Apagando equipo...");
	}
}
