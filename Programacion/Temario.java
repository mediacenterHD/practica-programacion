import java.util.ArrayList;
import java.util.Scanner;

public class Temario {

	private String Nombre;
	private ArrayList<Tema> Temario = new ArrayList<Tema>();
	private int HorasTotales;

	public void set_temario_name(String nombre) {
		this.Nombre = nombre.toUpperCase();
	}

	public Temario(String nombre) {
		this.Nombre = nombre.toUpperCase();
		set_temario();
	}

	public int horas_temario() {
		int HorasTotales = 0;
		for (int i = 0; i < Temario.size(); i++) {
			HorasTotales = HorasTotales + Temario.get(i).horas_tema();
		}
		this.HorasTotales = HorasTotales;
		return HorasTotales;
	}

	public void set_temario() {
		Scanner sc = new Scanner(System.in);
		String sino = "si";
		
		while (sino.equals("si")) {
			
			System.out.println("Introduce el nombre del tema que insertar");
			
			String nombre_tema = sc.nextLine();
			boolean exist_tema = false;
			for (Tema temas : Temario) {
				if (temas.nombre_tema().equals(nombre_tema)) {
					exist_tema = true;
					break;
				}
			}
			if (exist_tema == false) {
				System.out.println("Introduce el n� de horas para este tema:");
				int horas_tema = Integer.parseInt(sc.nextLine());
				Tema tema = new Tema(nombre_tema, horas_tema);
				Temario.add(tema);
				System.out.println("Tema insertado en el temario!");
			} else {
				System.out.println("Ya existe el tema en el temario, puedes editarlo o eliminarlo si lo deseas");
			}
			System.out.println("Desea introducir otro tema? si/no: ");
			sino = sc.nextLine();
		}

	}

	public Tema buscar_tema(ArrayList<Tema> Temario, String nombre_tema) {

		for (Tema tema : Temario) {
			if (tema.nombre_tema().equals(nombre_tema)) {
				return tema;
			}
		}
		return null;

	}

	public void delete_tema() {
		System.out.println("Introduec el tema a eliminar: ");
		Scanner sc = new Scanner(System.in);
		String nombreTema = sc.nextLine();
		Tema tema_encontrado = buscar_tema(Temario, nombreTema);
		if (tema_encontrado == null) {
			System.out.println("No existe el tema especificado");
		} else {
			Temario.remove(tema_encontrado);
			System.out.println("Tema eliminado");
		}
	}

	public void show_temario() {
		for (Tema tema : Temario) {
			System.out.println(
					"Temario : " + this.Nombre + "|| Tema:" + tema.nombre_tema() + " " + tema.horas_tema() + " horas");
		}
	}

	public void edit_tema() {
		System.out.println("Introduce el nombre del tema que quieres editar: ");
		Scanner sc = new Scanner(System.in);
		String nombre_tema_buscar = sc.nextLine();
		Tema tema_encontrado = buscar_tema(Temario, nombre_tema_buscar);
		if (tema_encontrado == null) {
			System.out.println("No existe el tema especificado, deseas crearlo?si/no");
			sc = new Scanner(System.in);
			String sino = sc.nextLine();
			if (sino.toLowerCase().equals("si")) {
				set_temario();
			}
		} else {
			int pos_tema = Temario.indexOf(tema_encontrado);
			System.out.println("Deseas cambiar el nombre? si/no:");
			String sino = sc.nextLine();
			if (sino.toLowerCase().equals("si")) {
				System.out.println("Introduce el nuevo nombre para el tema :");
				String nombreTema1 = sc.nextLine();
				Temario.get(pos_tema).set_nombre(nombreTema1);
			} else {
				System.out.println("Deseas editar el n� de horas? si/no");
				sino = sc.nextLine();
				if (sino.toLowerCase().equals("si")) {
					System.out.println("Introduce el n� de horas para el tema:");
					int numhoras = Integer.parseInt(sc.nextLine());
					Temario.get(pos_tema).set_hora(numhoras);
				}
			}
			System.out.println("Tema Editado");
		}
	}
}
